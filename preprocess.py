import os
from os.path import join, dirname


def read(path):
    with open(path, encoding="utf-8") as f:
        content = f.read()
    return content


def write(data, path):
    with open(path, "w") as f:
        content = "\n\n".join(i for i in data)
        f.write(content)


def convert_data(file):
    sequences = read(file).split("\n\n")
    sequences = [seq for seq in sequences if len(seq) > 1]
    return sequences


if __name__ == '__main__':
    files = []
    path = join(dirname(__file__), "asset", "raw")
    for i in ["train.txt", "dev.txt", 'test.txt']:
        file_name = [join(path, topic, i) for topic in os.listdir(path)]
        data = [convert_data(file) for file in file_name]
        data = [i for sublist in data for i in sublist]
        filename = join(dirname(__file__), "asset", "data", i)
        write(data, filename)
