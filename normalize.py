import re
import sys
import unicodedata

from pyvi.ViTokenizer import tokenize

vietnamese_alphabet = "aăâbcdđeêfghijklmnoôơpqrstuưvwxyz"
vietnamese_tone_alphabet = "áàảãạâấầẩẫậăắằẳẵặèéẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵ"
punctuation = " +/$.,:-"
digit = "0123456789"
characters = "".join((vietnamese_alphabet, vietnamese_tone_alphabet, punctuation, digit))


def Text(text):
    """ provide a wrapper for python string
    map byte to str (python 3)
    map str to unicode (python 2)
    all string in utf-8 encoding
    normalize string to nFC
    """
    if not is_unicode(text):
        text = text.decode("utf-8")
    text = unicodedata.normalize("NFC", text)
    return text


def is_unicode(text):
    if sys.version_info >= (3, 0):
        unicode_type = str
    else:
        unicode_type = unicode
    return type(text) == unicode_type


def remove_emoji(string):
    emoji_pattern = re.compile("["
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               u"\U00002702-\U000027B0"
                               u"\U000024C2-\U0001F251"
                               "]+", flags=re.UNICODE)
    return emoji_pattern.sub(r' ', string)


def remove_datetime(text):
    datetime = [
        r"\d{1,2}\/\d{1,2}(\/\d+)?",
        r"\d{1,2}-\d{1,2}(-\d+)?",
    ]
    pattern = re.compile(r"(" + "|".join(datetime) + ")")
    regex = pattern.findall(text)
    matched = ["%s" % token[0].strip(' ') for token in regex]
    text = " ".join(i for i in text.split() if i not in matched)
    return text


def remove_pattern(text):
    email = r"[-_.0-9A-Za-z]{1,64}@[-_0-9A-Za-z]{1,255}[-_.0-9A-Za-z]{1,255}"
    text = re.sub(email, " ", text)

    web = r"https?://[-_.?&~;+=/#0-9A-Za-z]{1,2076}"
    text = re.sub(web, " ", text)

    multi_char = r"[\-]{3,}"
    text = re.sub(multi_char, " ", text)

    text = text.replace("_", " ")
    text = text.replace(".", " ")
    text = text.replace("#", " ")

    split_text = [i for i in list(text) if i in characters]
    text = "".join(x for x in split_text)

    text = " ".join(i for i in text.split())
    return text


def replace_acronym(text):
    dictionary = [{"raw": u" nctt ", "replace": u" nghiên cứu thị trường "},
                  {"raw": u" cntt ", "replace": u" công nghệ thông tin "},
                  {"raw": u" hdv ", "replace": u" hướng dẫn viên"},
                  {"raw": u" mkt ", "replace": u" marketing "},
                  {"raw": u" cskh ", "replace": u" chăm sóc khách hàng "},
                  {"raw": u" lđpt ", "replace": u" lao động phổ thông"},
                  {"raw": u" qhkh ", "replace": u" quan hệ khách hàng "},
                  {"raw": u" lí ", "replace": u" lý "},
                  {"raw": u" kd ", "replace": u" kinh doanh "},
                  {"raw": u" nv ", "replace": u" nhân viên "},
                  {"raw": u" sx ", "replace": u" sản xuất "},
                  {"raw": u" pg ", "replace": u" promotion girl "},
                  {"raw": u" qlcl ", "replace": u" quản lý chất lượng "},
                  {"raw": u" pm ", "replace": u" project manager "},
                  {"raw": u" bắc từ niêm ", "replace": u" bắc từ liêm "},
                  {"raw": u" hp ", "replace": u" hải phòng "},
                  {"raw": u" ha noi ", "replace": u" hà nội "},
                  {"raw": u" hn ", "replace": u" hà nội "},
                  {"raw": u"hcm", "replace": u" hồ chí minh "},
                  {"raw": u"ho chi minh", "replace": u" hồ chí minh "},
                  {"raw": u"hồ chí minh", "replace": u" hồ chí minh "},
                  {"raw": u"daklak", "replace": u" đắk lắk "},
                  ]
    for item in dictionary:
        content = text.lower()
        if item["raw"] in content:
            content = content.replace(item["raw"], item["replace"])
            text = content
    return text


def normalize(raw_text):
    text = Text(raw_text)
    text = remove_emoji(text)
    text = remove_datetime(text)
    text = " ".join(i for i in text.split()).lower()
    text = remove_pattern(text)
    text = replace_acronym(text)
    text = tokenize(text)
    return text
