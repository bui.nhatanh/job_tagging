# Hướng dẫn gán nhãn thực thể

Mô hình gán nhãn thực thể với các thử nghiệm sử dụng dữ liệu: có chunking hoặc không có chunking.

### Yêu cầu hệ thống

Cấu hình tối thiểu

```
CPU: Core i5
RAM: 8 GB
```
Cấu hình đề nghị

```
CPU: Core i7
RAM: 16 GB
GPU
```
Mô trường cần thiết
```
Python 3.6+
Anaconda 3
Tensoflow 
```

### Dữ liệu

- Dữ liệu đưa vào gồm 2 file: text và annotition đã được chunking hoặc chưa được chuking được lưu tại thư mục raw
- Dữ liệu thử nghiệm tại thư mục corpus gồm 3 file train, dev, test
- Các file text khác tại thư mục data là dữ liệu cần thiết cho quá trình huấn luyện hoặc sinh ra sau quá trình huấn luyện (KHÔNG ĐƯỢC XÓA).

### Hướng dẫn sử dụng

**Bước 1**: Khởi tạo dữ liệu huấn luyện

Chú ý: chắc chắn rằng có các file: `glove.50d.txt`, `paths.txt` trong thư mục data
```
$ python preprocess.py
```
**Bước 2**: Copy file trong thư mục data trừ thư mục raw vào các thử nghiệm trong `egs`