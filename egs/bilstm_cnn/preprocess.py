from os.path import join, dirname


def read(filename):
    with open(filename) as f:
        content = f.read().split("\n")
    split_size = int(len(content) * 0.3)
    return content[:split_size]


data_path = join(dirname(dirname(dirname(__file__))), "asset", "data")
corpus_path = join(dirname(__file__), "data")
for name in ["train.txt", "dev.txt", "test.txt"]:
    file_name = join(data_path, name)
    corpus_file = join(corpus_path, name)
    with open(corpus_file, "w") as f:
        data = read(file_name)
        for line in data:
            if len(line) > 0:
                token = [line.split("\t")[0], line.split("\t")[3]]
                text = " ".join(i for i in token)
                f.write(text + "\n")
            else:
                f.write("\n")
