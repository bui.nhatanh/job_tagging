from model.data_utils import CoNLLDataset, get_vocabs, UNK, NUM, PUNCT, write_vocab, get_wordemb_vocab, \
    get_char_vocab, get_processing_word, get_brown_vocab

from model.config import Config


def main():
    config = Config(load=False)
    processing_word = get_processing_word(lowercase=True)

    dev = CoNLLDataset(config.filename_dev, processing_word)
    test = CoNLLDataset(config.filename_test, processing_word)
    train = CoNLLDataset(config.filename_train, processing_word)

    vocab_words, vocab_poss, vocab_chunks, vocab_tags = get_vocabs([train, dev, test])
    vocab_glove = get_wordemb_vocab(config.filename_glove)
    vocab_brown = get_brown_vocab(config.filename_brown)
    vocab = vocab_words & vocab_glove & vocab_brown
    vocab.add(UNK)
    vocab.add(NUM)
    vocab.add(PUNCT)

    write_vocab(vocab, config.filename_words)
    write_vocab(vocab_poss, config.filename_poss)
    write_vocab(vocab_chunks, config.filename_chunks)
    write_vocab(vocab_tags, config.filename_ners)

    train = CoNLLDataset(config.filename_train)
    dev = CoNLLDataset(config.filename_dev)
    test = CoNLLDataset(config.filename_test)
    vocab_chars = get_char_vocab([train, dev, test])
    write_vocab(vocab_chars, config.filename_chars)


if __name__ == '__main__':
    main()

