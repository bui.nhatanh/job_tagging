from .general_utils import get_logger
from .data_utils import *
import os


class Config():
    def __init__(self, load=True):
        if not os.path.exists(self.path_log):
            os.makedirs(self.dir_output)
        self.logger = get_logger(self.path_log)

        if load:
            self.load()

    def load(self):
        self.vocab_words = load_vocab(self.filename_words)
        self.vocab_poss = load_vocab(self.filename_poss)
        self.vocab_chunks = load_vocab(self.filename_chunks)
        self.vocab_ners = load_vocab(self.filename_ners)
        self.vocab_chars = load_vocab(self.filename_chars)

        self.nwords = len(self.vocab_words)
        self.nchars = len(self.vocab_chars)
        self.nposs = len(self.vocab_poss)
        self.nchunks = len(self.vocab_chunks)
        self.nners = len(self.vocab_ners)

        self.processing_word = get_processing_word(self.vocab_words, self.vocab_chars, chars=True, lowercase=True)
        self.processing_pos = get_processing_word(self.vocab_poss, lowercase=False, allow_unk=False)
        self.processing_chunk = get_processing_word(self.vocab_chunks, lowercase=False, allow_unk=False)
        self.processing_ner = get_processing_word(self.vocab_ners, lowercase=False, allow_unk=False)

        self.embeddings = load_word_embeddings(self.vocab_words, self.filename_glove, self.dim_word)
        self.clusterings = load_brown_clustering(self.vocab_words, self.filename_brown)
        self.poss_embeddings = load_semantic_embedding(self.vocab_poss, len(self.vocab_poss))
        self.chunks_embeddings = load_semantic_embedding(self.vocab_chunks, len(self.vocab_chunks))

    dir_output = "trained_model/"
    dir_model = dir_output + "model_ner.weights/"
    dir_model_pos = dir_output + "model_pos.weights/"
    path_log = dir_output + "log.txt"

    filename_brown = "data/paths.txt"
    filename_glove = "data/glove.50d.txt"

    filename_dev = "data/dev.txt"
    filename_test = "data/test.txt"
    filename_train = "data/train.txt"

    filename_words = "data/words.txt"
    filename_poss = "data/poss.txt"
    filename_chunks = "data/chunks.txt"
    filename_ners = "data/ners.txt"
    filename_chars = "data/chars.txt"

    max_iter = None

    mode_pos = False
    mode_ner = True
    use_brown = False
    use_semantic = False

    dim_word = 50
    dim_char = 100
    hidden_size_char = 100
    hidden_size_lstm = 150
    nepochs = 20
    dropout = 0.65
    batch_size = 4
    lr_method = "adam"
    lr = 0.0035
    lr_decay = 0.06
    nepoch_no_imprv = 0
