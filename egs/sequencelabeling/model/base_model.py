import os
import tensorflow as tf


class BaseModel(object):

    def __init__(self, config):

        self.config = config
        self.logger = config.logger
        self.sess = None
        self.saver = None

    def reinitialize_weights(self, scope_name):
        variables = tf.contrib.framework.get_variables(scope_name)
        init = tf.variables_initializer(variables)
        self.sess.run(init)

    def add_train_op(self, lr_method, lr, loss):
        _lr_m = lr_method.lower()
        with tf.variable_scope("train_step"):
            if _lr_m == 'adam':
                optimizer = tf.train.AdamOptimizer(lr)
            elif _lr_m == 'adagrad':
                optimizer = tf.train.AdagradOptimizer(lr)
            elif _lr_m == 'sgd':
                optimizer = tf.train.GradientDescentOptimizer(lr)
            elif _lr_m == 'rmsprop':
                optimizer = tf.train.RMSPropOptimizer(lr)
            else:
                raise NotImplementedError("Unknown method {}".format(_lr_m))

            self.train_op = optimizer.minimize(loss)

    def initialize_session(self):
        self.logger.info("Initializing tf session")
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        self.saver = tf.train.Saver()

    def restore_session(self, dir_model):

        self.logger.info("Reloading the latest trained model...")
        self.saver.restore(self.sess, dir_model)

    def save_session(self, dir_model):
        if not os.path.exists(dir_model):
            os.makedirs(dir_model)
        self.saver.save(self.sess, dir_model)

    def close_session(self):
        """Closes the sessin"""
        self.sess.close()

    def train(self, train, dev, test):

        best_score = 0
        nepoch_no_imprv = 0  # for early stopping

        for epoch in range(self.config.nepochs):
            self.logger.info("Epoch {:} out of {:}".format(epoch + 1,
                                                           self.config.nepochs))

            score = self.run_epoch(train, dev, epoch)
            self.config.lr = self.config.lr / (1 + epoch * self.config.lr_decay)  # decay learning rate
            # early stopping and saving best parameters
            if score >= best_score:
                nepoch_no_imprv = 0
                if self.config.mode_pos:
                    self.save_session(self.config.dir_model_pos)
                    metrics = self.run_evaluate(test)
                    msg = " - ".join(["{}".format(k) for k in metrics.items()])

                if self.config.mode_ner:
                    self.save_session(self.config.dir_model)
                    metrics = self.run_evaluate(test)
                    msg = " - ".join(["{}:{}".format(k, v) for k, v in metrics.items()])
                    self.save_session(self.config.dir_model)
                self.logger.info(msg)
                best_score = score
                self.logger.info("- new best score!")
            else:
                nepoch_no_imprv += 1
                if nepoch_no_imprv >= self.config.nepoch_no_imprv:
                    self.logger.info("- early stopping {} epochs without " \
                                     "improvement".format(nepoch_no_imprv))
                    break
