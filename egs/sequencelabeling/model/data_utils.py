import numpy as np
import string

UNK = "<unk>"
NUM = "<number>"
PUNCT = "<punct>"
NONE = "O"


class MyIOError(Exception):
    def __init__(self, filename):
        message = """
ERROR: Unable to locate file {}.

FIX: Have you tried running python update.py first?
This will build vocab file from your train, test and dev sets and
trimm your word vectors.
""".format(filename)
        super(MyIOError, self).__init__(message)


class CoNLLDataset(object):

    def __init__(self, filename, processing_word=None, processing_pos=None, processing_chunk=None,
                 processing_ner=None, max_iter=None):

        self.filename = filename
        self.processing_word = processing_word
        self.processing_pos = processing_pos
        self.processing_chunk = processing_chunk
        self.processing_ner = processing_ner
        self.max_iter = max_iter
        self.length = None

    def __iter__(self):
        niter = 0
        with open(self.filename) as f:
            words, poss, chunks, ners = [], [], [], []
            for line in f:
                line = line.strip()
                if len(line) == 0:
                    if len(words) != 0:
                        niter += 1
                        if self.max_iter is not None and niter > self.max_iter:
                            break
                        yield words, poss, chunks, ners
                        words, poss, chunks, ners = [], [], [], []
                else:
                    ls = line.split('\t')
                    word, pos, chunk, ner = ls[0], ls[1], ls[2], ls[3]
                    if self.processing_word is not None:
                        word = self.processing_word(word)
                    if self.processing_pos is not None:
                        pos = self.processing_pos(pos)
                    if self.processing_chunk is not None:
                        chunk = self.processing_chunk(chunk)
                    if self.processing_ner is not None:
                        ner = self.processing_ner(ner)
                    words += [word]
                    poss += [pos]
                    chunks += [chunk]
                    ners += [ner]

    def __len__(self):
        """Iterates once over the corpus to set and store length"""
        if self.length is None:
            self.length = 0
            for _ in self:
                self.length += 1

        return self.length


def get_vocabs(datasets):
    print("Building vocab...")
    vocab_words = set()
    vocab_poss = set()
    vocab_chunks = set()
    vocab_ners = set()
    for dataset in datasets:
        for words, poss, chunks, ners in dataset:
            vocab_words.update(words)
            vocab_poss.update(poss)
            vocab_chunks.update(chunks)
            vocab_ners.update(ners)
    print("- done. {} tokens".format(len(vocab_words)))
    print("- done. {} pos".format(len(vocab_poss)))
    print("- done. {} chunks".format(len(vocab_chunks)))
    return vocab_words, vocab_poss, vocab_chunks, vocab_ners


def get_char_vocab(datasets):
    vocab_char = set()
    for dataset in datasets:
        for words, _, _, _ in dataset:
            for word in words:
                vocab_char.update(word)

    return vocab_char


def get_wordemb_vocab(filename):
    """Load vocab from file

    Args:
        filename: path to the glove vectors

    Returns:
        vocab: set() of strings
    """
    print("Building vocab glove...")
    vocab = set()
    with open(filename) as f:
        for line in f:
            word = line.strip().split(' ')[0]
            vocab.add(word)
    print("- done. {} tokens".format(len(vocab)))
    return vocab


def get_brown_vocab(filename):
    print("Building vocab clustering...")
    vocab = set()
    with open(filename) as f:
        for line in f:
            word = line.strip().split()[1]
            vocab.add(word)
    print("- done. {} tokens".format(len(vocab)))
    return vocab


def write_vocab(vocab, filename):
    print("Writing vocab...")
    with open(filename, "w") as f:
        for i, word in enumerate(vocab):
            if i != len(vocab) - 1:
                f.write("{}\n".format(word))
            else:
                f.write(word)
    print("- done. {} tokens".format(len(vocab)))


def load_vocab(filename):
    try:
        d = dict()
        with open(filename) as f:
            for idx, word in enumerate(f):
                word = word.strip()
                d[word] = idx

    except IOError:
        raise MyIOError(filename)
    return d


def parse_file(filename):
    raw_text = open(filename, 'r').readlines()
    sentences = []
    s = []
    for line in raw_text:
        broken = line.strip().split()
        if line.strip():
            if len(broken) == 4:
                s.append(broken)
        else:
            sentences.append(s)
            s = []
    return sentences


def load_brown_clustering(vocab, brown_filename):
    max_length = 0
    with open(brown_filename) as f:
        for line in f:
            line = line.strip().split()
            if len(line[0]) > max_length:
                max_length = len(line[0])
    clusterings = np.zeros([len(vocab), max_length])
    with open(brown_filename) as f:
        for line in f:
            line = line.strip().split()
            word = line[1]
            clustering = [int(x) for x in list(line[0])]
            clustering = [0] * max(max_length - len(clustering), 0) + clustering
            if word in vocab:
                word_idx = vocab[word]
                clusterings[word_idx] = np.asarray(clustering)
    return clusterings


def load_word_embeddings(vocab, wordemb_filename, dim):
    embeddings = np.zeros([len(vocab), dim])
    with open(wordemb_filename) as f:
        for line in f:
            line = line.strip().split(' ')
            word = line[0]
            embedding = [float(x) for x in line[1:]]
            if word in vocab:
                word_idx = vocab[word]
                embeddings[word_idx] = np.asarray(embedding)
    return embeddings


def load_semantic_embedding(vocab, dim):
    embeddings = np.zeros([len(vocab), dim])
    for tag in vocab:
        word_idx = vocab[tag]
        embeddings[word_idx][word_idx] = 1.0
    return embeddings


def get_processing_word(vocab_words=None, vocab_chars=None,
                        lowercase=False, chars=False, allow_unk=True):
    def f(word):
        # 0. get chars of words
        if vocab_chars is not None and chars == True:
            char_ids = []
            for char in word:
                # ignore chars out of vocabulary
                if char in vocab_chars:
                    char_ids += [vocab_chars[char]]

        # 1. preprocess word
        if lowercase:
            word = word.lower()
        if word[0].isdigit():
            word = NUM
        if word in string.punctuation and word != "_":
            word = PUNCT

        # 2. get id of word
        if vocab_words is not None:
            if word in vocab_words:
                word = vocab_words[word]
            else:
                if allow_unk:
                    word = vocab_words[UNK]
                else:
                    raise Exception("Unknow key is not allowed. Check that your vocab (ners?) is correct")

        # 3. return tuple char ids, word id
        if vocab_chars is not None and chars == True:
            return char_ids, word
        else:
            return word

    return f


def _pad_sequences(sequences, pad_tok, max_length):
    sequence_padded, sequence_length = [], []

    for seq in sequences:
        seq = list(seq)
        seq_ = seq[:max_length] + [pad_tok] * max(max_length - len(seq), 0)
        sequence_padded += [seq_]
        sequence_length += [min(len(seq), max_length)]

    return sequence_padded, sequence_length


def pad_sequences(sequences, pad_tok, nlevels=1):
    if nlevels == 1:
        max_length = max(map(lambda x: len(x), sequences))
        sequence_padded, sequence_length = _pad_sequences(sequences,
                                                          pad_tok, max_length)

    elif nlevels == 2:
        max_length_word = max([max(map(lambda x: len(x), seq))
                               for seq in sequences])
        sequence_padded, sequence_length = [], []
        for seq in sequences:
            sp, sl = _pad_sequences(seq, pad_tok, max_length_word)
            sequence_padded += [sp]
            sequence_length += [sl]

        max_length_sentence = max(map(lambda x: len(x), sequences))
        sequence_padded, _ = _pad_sequences(sequence_padded,
                                            [pad_tok] * max_length_word, max_length_sentence)
        sequence_length, _ = _pad_sequences(sequence_length, 0,
                                            max_length_sentence)

    return sequence_padded, sequence_length


def minibatches(data, minibatch_size):
    w_batch, p_batch, c_batch, n_batch = [], [], [], []
    for (w, p, c, n) in data:
        if len(w_batch) == minibatch_size:
            yield w_batch, p_batch, c_batch, n_batch
            w_batch, p_batch, c_batch, n_batch = [], [], [], []

        if type(w[0]) == tuple:
            w = zip(*w)
        w_batch += [w]
        p_batch += [p]
        c_batch += [c]
        n_batch += [n]

    if len(w_batch) != 0:
        yield w_batch, p_batch, c_batch, n_batch


def get_chunk_type(tok, idx_to_tag):
    tag_name = idx_to_tag[tok]
    tag_class = tag_name.split('-')[0]
    tag_type = tag_name.split('-')[-1]
    return tag_class, tag_type


def get_chunks(seq, ners):
    default = ners[NONE]
    idx_to_tag = {idx: tag for tag, idx in ners.items()}
    chunks = []
    chunk_type, chunk_start = None, None
    for i, tok in enumerate(seq):
        if tok == default and chunk_type is not None:
            chunk = (chunk_type, chunk_start, i)
            chunks.append(chunk)
            chunk_type, chunk_start = None, None

        elif tok != default:
            tok_chunk_class, tok_chunk_type = get_chunk_type(tok, idx_to_tag)
            if chunk_type is None:
                chunk_type, chunk_start = tok_chunk_type, i
            elif tok_chunk_type != chunk_type or tok_chunk_class == "B":
                chunk = (chunk_type, chunk_start, i)
                chunks.append(chunk)
                chunk_type, chunk_start = tok_chunk_type, i
        else:
            pass
    if chunk_type is not None:
        chunk = (chunk_type, chunk_start, len(seq))
        chunks.append(chunk)

    return chunks
