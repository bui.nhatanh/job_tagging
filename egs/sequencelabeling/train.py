from model.data_utils import CoNLLDataset
from model.ner_model import NERModel

from model.config import Config


def main():
    config = Config()
    model = NERModel(config)

    dev = CoNLLDataset(config.filename_dev, config.processing_word, config.processing_pos, config.processing_chunk,
                       config.processing_ner, config.max_iter)
    train = CoNLLDataset(config.filename_train, config.processing_word, config.processing_pos, config.processing_chunk,
                         config.processing_ner, config.max_iter)
    test = CoNLLDataset(config.filename_test, config.processing_word, config.processing_pos,
                        config.processing_chunk, config.processing_ner, config.max_iter)

    model.build()
    model.train(train, dev, test)


if __name__ == '__main__':
    main()
