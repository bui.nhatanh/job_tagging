from egs.sequencelabeling.helpers import take_entities
from egs.sequencelabeling.model.config import Config
from egs.sequencelabeling.model.ner_model import NERModel

config = Config()
model = NERModel(config)
model.config.mode_ner = True
model.build()
model.restore_session(config.dir_model)


def entities_recognition(text):
    sentence = text.strip().split()
    if len(sentence) > 0:
        preds = model.predict(sentence)
        tokens = list(zip(sentence, preds))
        return tokens
    else:
        return []


def add_entity(rows):
    keys = ["JOB", "LOCATION", "SALARY"]
    tags = [list(item.keys())[0] for item in rows]
    missing_tag = [i for i in keys if i not in tags]
    for key in missing_tag:
        new_row = {}
        new_row[key] = []
        rows.append(new_row)
    return rows


def sequence_extractor(text):
    tokens = entities_recognition(text)
    tokens.append(("", "O"))
    entities = take_entities(tokens)
    entities = add_entity(entities)
    return entities
