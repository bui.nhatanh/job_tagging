import collections
import operator
from functools import reduce


def combine(entities):
    result = []
    if not entities:
        return list()
    else:
        keys = reduce(operator.or_, (entity.keys() for entity in entities))
        combine_entity = {key: [d.get(key) for d in entities] for key in keys}
        for key, value in combine_entity.items():
            value = list(set([i for i in value if i is not None]))
            entity = {"{}".format(key): value}
            result.append(entity)
    return result


def take_entities(tokens):
    entities = []
    text = ""
    flag = 0
    for i in tokens:
        if i[1].startswith("B") and flag == 1:
            entities.append({type: text})
            text = i[0]
            type = i[1].split("B-")[1]
            flag = 1
        elif i[1].startswith("I") and flag == 1:
            text = text + " " + i[0]
        elif i[1].startswith("B"):
            text = i[0]
            type = i[1].split("B-")[1]
            flag = 1
        elif flag == 1 and not i[1].startswith("I"):
            entities.append({type: text})
            flag = 0
            text = ""
        else:
            text = 0
    output = combine(entities)
    return output
